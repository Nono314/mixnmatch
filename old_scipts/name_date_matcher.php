#!/usr/bin/php
<?PHP

function verifyDates ( $q , $year1 , $year2 ) {
	$q = "Q$q" ;
//	print "TESTING $q , $year1 , $year2\n" ;
	
	$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !isset($j->entities->$q) ) return false ;
	if ( !isset($j->entities->$q->claims) ) return false ;
	$claims = $j->entities->$q->claims ;
	if ( !isset($claims->P569) || !isset($claims->P570) ) return false ;
	
//	print_r ( $claims->P569 ) ;
	
	$found = false ;
	foreach ( $claims->P569 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year1.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;

//	print_r ( $claims->P570 ) ;

	$found = false ;
	foreach ( $claims->P570 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year2.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;
	
	return true ;
}

require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$cnt = 0 ;
$sql = "select * from entry where user=0 and q is not null and ext_desc LIKE '%(%)%'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !preg_match ( '/(\d{4,}).*(\d{4,})/' , $o->ext_desc , $m ) ) continue ;
	if ( $m[1]*1>2020 or $m[2]*1>2020 ) continue ; // Paranoia
	if ( !verifyDates ( $o->q , $m[1] , $m[2] ) ) continue ;
	$sql = "UPDATE entry SET user=3 WHERE id=" . $o->id ;
//	print "$sql\n" ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '.$sql."\n");
	$cnt++ ;
}

print "$cnt assigned\n" ;

?>