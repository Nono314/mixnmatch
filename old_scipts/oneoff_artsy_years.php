#!/usr/bin/php
<?PHP
require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$sql = "SELECT * FROM entry WHERE catalog=52 AND ext_desc='' AND (q is null or q < 1 or user=0) AND type='person'" ;
//$sql = "SELECT * FROM entry WHERE id=3132890" ; // TESTING

$os = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$os[] = $o ;
}

foreach ( $os AS $o ) {
	$h = file_get_contents ( $o->ext_url ) ;
	$desc = array() ;
	if ( preg_match ( '/<meta property="og:birthyear" content="(\d+)">/' , $h , $m ) ) { $desc[] = "born " . $m[1] ; }
	if ( preg_match ( '/<meta property="og:deathyear" content="(\d+)">/' , $h , $m ) ) { $desc[] = "died " . $m[1] ; }
	if ( count($desc) == 0 ) continue ;
	$desc = implode ( '; ' , $desc ) ;
	$sql = "UPDATE entry SET ext_desc='" . $db->real_escape_string($desc) . "' WHERE ext_desc='' and id=" . $o->id ;
//	print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

?>