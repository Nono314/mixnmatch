#!/usr/bin/php
<?PHP
require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$sql = "SELECT * FROM entry WHERE catalog=23 AND (q is null or q < 1 or user=0)" ;
#$sql .= " AND id=850901" ;

$candidates = array() ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$n = strtolower ( $o->ext_name ) ;
	if ( !preg_match ( '/^[a-z]+ [a-z]+$/' , $n ) ) continue ;
	$candidates[] = $o ;
}

foreach ( $candidates AS $o ) {
	$n = strtolower ( $o->ext_name ) ;
	$url = "$wdq_internal_url?q=" . urlencode('string[225:"'.$n.'"] or string[225:"'.ucfirst($n).'"]') ;
#	print "$url\n" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count ( $j->items ) != 1 ) continue ;
	$q = $j->items[0] ;
	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET q=$q,user=4,timestamp='$ts' WHERE id=" . $o->id ;
	print "$sql\n" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
#	print_r ( $j ) ;
}

?>