#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

$double_aux_p = array() ;
$sql = "select aux_p,aux_name,count(*) AS cnt from `auxiliary` group by aux_p,aux_name having cnt>1" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$double_aux_p[$o->aux_p] = $o->aux_p ;
}



if ( 1 ) { // Find aux data on Wikidata, then update mix'n'match
	$sql = "SELECT *,auxiliary.id AS aid FROM auxiliary,entry WHERE entry_id=entry.id AND in_wikidata=0" ;
	if ( count($double_aux_p) > 0 ) $sql .= " AND aux_p NOT IN (" . implode(',',$double_aux_p) . ")" ;
$sql .= " AND aux_p=214" ; # TESTING
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$query = "STRING[".$o->aux_p.':"'.$o->aux_name.'"]' ;
		$url = "$wdq_internal_url?q=".urlencode($query) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		if ( count($j->items) == 0 ) continue ; // Ah well...
		if ( count($j->items) > 1 ) {
			print "Multiple items for " . $o->aux_p . ":" . $o->aux_name . " are " . implode(',',$j->items) . "\n" ;
			continue ;
		}
		$q = $j->items[0] ;
		if ( $q == $o->q ) { // We already know
			$sql = "UPDATE /*1*/ auxiliary SET in_wikidata=1 WHERE id=" . $o->aid ;
			print "$sql\n" ;
			if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
			if ( $o->user == 0 ) {
				$sql = "UPDATE entry SET user=4 WHERE id=" . $o->entry_id ;
				print "$sql\n" ;
				if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
			}
			continue ;
		}
		if ( $o->q != '' ) {
			print "CONTRADICTION " . $o->q . " / $q\n" ;
			continue ;
		}
	
		$ts = date ( 'YmdHis' ) ;
		$sql = "UPDATE /*2*/ entry SET q=$q,`timestamp`='$ts',user=4 WHERE id=" . $o->entry_id ;
		print "$sql\n" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$sql = "UPDATE /*3*/ auxiliary SET in_wikidata=1 WHERE id=" . $o->aid ;
		print "$sql\n" ;
		if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	}
}

$fh = fopen ( 'public_html/aux_sync.txt' , 'w' ) ;
$sql = "SELECT *,auxiliary.id AS aid FROM auxiliary,entry WHERE entry_id=entry.id AND in_wikidata=0 AND user>3" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( preg_match ( '/"/' , $o->aux_name ) ) continue ; // Double quote hell
	fwrite ( $fh , "Q".$o->q."\tP".$o->aux_p."\t\"".$o->aux_name."\"\n" ) ;
}
fclose ( $fh ) ;

?>