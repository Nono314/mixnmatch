#!/usr/bin/php
<?PHP

exit ( 0 ) ; // DEACTIVATED FOR ODD RESULTS

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog = $argv[1] * 1 ;
$en_only = false ;

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'public_html/php/wikidata.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

function getSearch ( $query ) {
	return json_decode ( file_get_contents ( "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ) ) ;
}

$db = openMixNMatchDB() ;

$cat = (object) array() ;
$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()) $cat = $o ;
$lang = $cat->search_wp ;

$id2data = array() ;
$sql = "SELECT * FROM entry WHERE catalog=$catalog and q IS NULL" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$id2data[$o->id] = $o ;
}

$candidates = array() ;
foreach ( $id2data AS $o ) {
	$search = array ( $o->ext_name ) ;
	if ( preg_match_all ( '/\b(\d{1,2}\s[a-zA-Z]+\s\d{3,4})\b/' , $o->ext_desc , $m ) ) {
		foreach ( $m[0] AS $date ) $search[] = $date ;
	}
	if ( preg_match_all ( '/\b(\d{3,4})\b/' , $o->ext_desc , $m ) ) {
		foreach ( $m[0] AS $date ) $search[] = $date ;
	}
	
	$search[] = $cat->betamatch_hint ;
	
	$search = trim ( implode ( ' ' , $search ) ) ;
	$url = "https://$lang.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=".urlencode($search) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$titles = array() ;
	foreach ( $j->query->search AS $r ) {
		if ( preg_match ( '/^List*/' , $r->title ) ) continue ;
		$titles[] = $r->title ;
	}
	if ( count ( $titles ) == 0 ) continue ;
	if ( count ( $titles ) > 5 ) continue ; // Too many
	
	$title = $titles[0] ;
	$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&sites=".$lang."wiki&titles=" . urlencode($title) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	$q = '' ;
	foreach ( $j->entities AS $id => $dummy ) $q = $id ;
	if ( $q == '' ) continue ; // No Wikidata item
	
	$i = $j->entities->$q ;
	if ( $o->type == 'person' ) {
		$wdi = new WDI ;
		$p31 = 'P31' ;
		if ( !isset($i->claims->$p31) ) continue ;
		$is_human = false ;
		foreach ( $i->claims->$p31 AS $claim ) {
			$q2 = $wdi->getTarget ( $claim ) ;
			if ( $q2 == 'Q5' ) $is_human = true ;
		}
		if ( !$is_human ) continue ;
	}
	
	$candidates[''.$o->id] = preg_replace ( '/\D/' , '' , $q ) ;
	print "$o->ext_name : $o->ext_desc => $q\n" ;
}


$db = openMixNMatchDB() ;

$ts = date ( 'YmdHis' ) ;
foreach ( $candidates AS $entry => $q ) {
	$sql = "UPDATE entry SET q=$q,user=0,timestamp='$ts' WHERE id=$entry AND q IS NULL" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry=entry.id)" ; # Prevent re-linking for manually unlinked items
#	$sql .= " AND NOT EXISTS (SELECT * FROM entry e2 WHERE e2.q=$q AND e2.catalog=$catalog)" ; # Prevent doubles # THIS DOES NOT WORK!!
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}

file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=$catalog" ) ; // Update stats

print count($candidates) . " entries auto-matched\n" ;

?>
