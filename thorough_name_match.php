#!/usr/bin/php
<?PHP

function verifyDates ( $q , $year1 , $year2 ) {
	$q = "Q$q" ;
//	print "TESTING $q , $year1 , $year2\n" ;
	
	$url = "http://www.wikidata.org/wiki/Special:EntityData/$q.json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !isset($j->entities->$q) ) return false ;
	if ( !isset($j->entities->$q->claims) ) return false ;
	$claims = $j->entities->$q->claims ;
	if ( !isset($claims->P569) || !isset($claims->P570) ) return false ;
	
//	print_r ( $claims->P569 ) ;
	
	$found = false ;
	foreach ( $claims->P569 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year1.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;

//	print_r ( $claims->P570 ) ;

	$found = false ;
	foreach ( $claims->P570 AS $v ) {
		$time = $v->mainsnak->datavalue->value->time ;
		if ( 1 == count ( explode ( $year2.'-' , $time ) ) ) continue ;
		$found = true ;
	}
	if ( !$found ) return false ;
	
	return true ;
}



if ( isset($argv[1]) ) $catalog = $argv[1] ; // Unset this to do all



require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

$db = openMixNMatchDB() ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;

$cnt = 0 ;
$sql = "select * from entry where (user=0 or user is null)" ;
$sql .= " AND ext_desc regexp binary '[0-9]{3,4}[^0-9]+[0-9]{3,4}'" ;
#$sql .= " and (ext_desc LIKE '%(%)%' or ext_desc like '%[0-9][0-9][0-9][0-9]%_%[0-9][0-9][0-9][0-9]%')" ;
$sql .= " AND NOT EXISTS (SELECT * FROM `log` WHERE log.entry=entry.id)" ; # Prevent re-linking for manually unlinked items
if ( isset ( $catalog ) ) $sql .= " AND catalog=$catalog" ;
#print "$sql\n" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !preg_match ( '/\b(\d{3,4})\D+(\d{3,4})\b/' , $o->ext_desc , $m ) ) continue ;
	if ( $m[1]*1>2020 or $m[2]*1>2020 ) continue ; // Paranoia
	$y1 = $m[1] ;
	$y2 = $m[2] ;
	
#	print $y1 . "/" . $y2 . "\t" . $o->ext_name . "\n" ;

//	print "--------------\n\n" ;	
//	print_r ( $o ) ;

	// Fix name
	$name = $o->ext_name ;
	$name = preg_replace ( '/ Bt$/' , ' Baronet' , $name ) ;
	
	$names = array ( $db->real_escape_string($name) ) ;
	if ( preg_match ( '/^Sir /' , $name ) ) {
		$n = preg_replace ( '/^Sir /' , '' , $name ) ;
		$n = preg_replace ( '/\s*\(.+\)/' , '' , $n ) ;
		$names[] = $db->real_escape_string($n) ;
	} else if ( preg_match ( '/^(\w+)\s(\w+)\s(\w+)$/' , $name , $m ) ) {
		$names[] = $db->real_escape_string ( $m[1] . ' ' . $m[3] ) ;
		$names[] = $db->real_escape_string ( $m[2] . ' ' . $m[3] ) ;
	}
	if ( preg_match ( '/^(Baron|Baronesse{0,1}|Graf|Gräfin) (.+)$/' , $name , $m ) ) {
		$names[] = $db->real_escape_string ( $m[1] ) ;
	}
	
	
	// Run query
	if ( !$dbwd->ping() ) $dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
	$sql = "SELECT DISTINCT terms.term_entity_id AS i FROM wb_terms AS terms WHERE " ;
	$sql .= " terms.term_type IN ('label','alias') and terms.term_text IN ('" . implode("','",$names) . "') and terms.term_entity_type='item'" ;
	$sql .= " and term_language='en' " ;
//	print "$sql\n" ;
	if(!$result2 = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	$items = array() ;
	while($o2 = $result2->fetch_object()){
		$items[$o2->i] = $o2->i ;
	}
//	print_r ( $items ) ;
	
	$query = "between[569,$y1-00-00,$y1-13-31] and between[570,$y2-00-00,$y2-13-31]" ;
//	print "$y1\t$y2\t$query\n" ;
	$url = "$wdq_internal_url?q=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
//	print_r ( $j ) ;
	
	$candidates = array() ;
	foreach ( $j->items AS $k => $v ) {
		if ( isset ( $items[$v] ) ) $candidates[] = $v ;
	}
	
	if ( count ( $candidates ) != 1 ) continue ;
	$q = array_pop ( $candidates ) ;
	
	$ts = date ( 'YmdHis' ) ;
	if ( !$db->ping() ) $db = openDB ( 'wikidata' , 'wikidata' ) ;
	$sql = "UPDATE entry SET user=3,q=$q,timestamp='$ts' WHERE id=" . $o->id ;
	if(!$result2 = $db->query($sql)) die('There was an error running the query [' . $db->error . ']: '.$sql);
	
	print $o->id . " => Q$q\n" ;
	
//	print "$sql\n" ;

	$cnt++ ;
//	break ;
}

print "$cnt assigned\n" ;

file_get_contents ( 'https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview' ) ; // Update stats

?>
