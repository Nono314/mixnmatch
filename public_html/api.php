<?PHP

ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
require_once ( '../opendb.inc' ) ; // $db = openMixNMatchDB() ;

//exit ( 0 ) ;

function update_overview ( $catalog ) {
	global $db , $out ;
	$catalogs = array () ;
	if ( isset ( $catalog ) ) {
		$catalogs[] = $catalog ;
	} else {
		$sql = "SELECT id from catalog" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
		while($o = $result->fetch_object()) $catalogs[] = $o->id ;
	}
	
	$out['catalogs'] = $catalogs ;
	foreach ( $catalogs AS $cat ) {
		$sql = "INSERT IGNORE INTO overview (catalog) VALUES ($cat)" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
		
		$sql = "UPDATE overview SET " ;
		$sql .= "total = (SELECT count(*) FROM entry WHERE catalog=$cat)," ;
		$sql .= "noq = (SELECT sum((case when isnull(`entry`.`q`) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
		$sql .= "autoq = (SELECT sum((case when (`entry`.`user` = 0) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
		$sql .= "na = (SELECT sum((case when (`entry`.`q` = 0) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
		$sql .= "manual = (SELECT sum((case when ((`entry`.`q` > 0) and (`entry`.`user` > 0)) then 1 else 0 end)) FROM entry WHERE catalog=$cat)," ;
		$sql .= "nowd = (SELECT sum((case when (`entry`.`q` = -(1)) then 1 else 0 end)) FROM entry WHERE catalog=$cat)" ;
		$sql .= " WHERE catalog=$cat" ;
		
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	}
}

$bad_q = array() ;
function compareSets ( $s1 , $s2 ) {
	global $bad_q ;
	$ret = array() ;
	foreach ( $s1 AS $q => $v1 ) {
		if ( isset($bad_q[$q*1]) ) continue ; // Duplicate horrors, ignore
		foreach ( $v1 AS $extid ) {
			if ( !isset ( $s2[$q] ) ) {
				$ret[] = array ( $q , utf8_encode($extid) ) ;
			} else if ( !in_array ( $extid , $s2[$q] ) ) {
				$ret[] = array ( $q , utf8_encode($extid) ) ;
			}
		}
	}
	return $ret ;
}

function ensureUserID ( $user , $project ) {
	global $db ;
	$sql = "INSERT IGNORE user (tusc_username,tusc_wiki) VALUES ('$user','$project')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$user_id = -1 ;
	$sql = "SELECT id FROM user WHERE tusc_username='$user' AND tusc_wiki='$project'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$user_id = $o->id ;
	}
	return $user_id ;
}

function get_users ( $users ) {
	global $db ;
	if ( count ( $users ) == 0 ) return array() ;
	$ret = array() ;
	$sql = "SELECT * FROM user WHERE id IN (" . implode(',',array_keys($users)) . ")" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$ret[$o->id] = $o ;
	}
	return $ret ;
}

function verify_tusc2 () {
	global $tusc_user ;
	if ( $tusc_user == '' ) return false ;
	return true ;
}

$query = get_request ( 'query' , '' ) ;
$tusc_user = get_request ( 'tusc_user' , -1 ) ;
$tusc_password = get_request ( 'tusc_pass' , -1 ) ;

$db = openMixNMatchDB() ;

if ( $query == 'download' ) {
	$filename = '' ;
	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$filename = str_replace ( ' ' , '_' , $o->name ) . ".txt" ;
	}
	header('Content-type: text/plain');
	header('Content-Disposition: attachment;filename="' . $filename . '"');
	print "Q\tID\tURL\tName\n" ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND q IS NOT NULL AND q > 0 AND user!=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$p = '' ;
		$p .= $o->{q} . "\t" ;
		$p .= $o->{ext_id} . "\t" ;
		$p .= $o->{ext_url} . "\t" ;
		$p .= $o->{ext_name} . "\n" ;
		print $p ;
#		print utf8_encode ( $p ) ;
	}
	exit ( 0 ) ;
}



$sql = "SET CHARACTER SET utf8" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');


if ( $query == 'redirect' ) {
	$catalog = get_request ( 'catalog' , '0' ) * 1 ;
	$ext_id = get_request ( 'ext_id' , '' ) ;
	$url = '' ;
	$sql = "SELECT ext_url FROM entry WHERE catalog=$catalog AND ext_id='" . $db->real_escape_string ( $ext_id ) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $url = $o->ext_url ;
	header('Content-type: text/html');
	print '<html><head><META http-equiv="refresh" content="0;URL='.$url.'"></head><body></body></html>' ;
	exit ( 0 ) ;
}



header('Content-type: application/json');

$out = array (
	'status' => 'OK' ,
	'data' => array()
) ;

if ( $query == 'catalogs' ) {

	$sql = "SELECT overview.* FROM overview,catalog WHERE catalog.id=overview.catalog and catalog.active=1" ; // overview is "manually" updated, but fast; vw_overview is automatic, but slow
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $out['data'][$o->catalog][$k] = $v ;
	}

	$sql = "SELECT * FROM catalog WHERE active=1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		foreach ( $o AS $k => $v ) $out['data'][$o->id][$k] = $v ;
	}

} else if ( $query == 'update_overview' ) {

	$catalog = get_request ( 'catalog' , '' ) ;
	if ( $catalog != '' ) update_overview ( $catalog ) ;
	else update_overview() ;

} else if ( $query == 'catalog' or $query == 'get_entry' ) {
	$entry = get_request ( 'entry' , -1 ) ;
	$id = get_request ( 'catalog' , -1 ) ;
	$meta = json_decode ( get_request ( 'meta' , '' ) ) ;
	
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	
	$q = array() ;
	

	$sql = "SELECT * FROM entry WHERE " ;
	if ( $query == 'get_entry' ) {
		$sql .= "id=" . $db->real_escape_string($entry) ;
	}else {
		$sql .= "catalog=" . $db->real_escape_string($id) ;
		if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_nowd == 0 and $meta->show_na == 1 ) {
			$sql .= " AND q=0" ;
		} else if ( $meta->show_noq+$meta->show_autoq+$meta->show_userq+$meta->show_na == 0 and $meta->show_nowd == 1 ) {
			$sql .= " AND q=-1" ;
		} else {
			if ( $meta->show_noq != 1 ) $sql .= " AND q IS NOT NULL" ;
			if ( $meta->show_autoq != 1 ) $sql .= " AND ( q is null OR user!=0 )" ;
			if ( $meta->show_userq != 1 ) $sql .= " AND ( user<=0 OR user is null )" ;
			if ( $meta->show_na != 1 ) $sql .= " AND ( q!=0 or q is null )" ;
//			if ( $meta->show_nowd != 1 ) $sql .= " AND ( q=-1 )" ;
		}
		$sql .= " ORDER BY ext_id" ;
		$sql .= " LIMIT " . $db->real_escape_string($meta->per_page) ;
		$sql .= " OFFSET " . $db->real_escape_string($meta->offset) ;
	}
	
	$out['sql'][] = $sql ;
	
	$users = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data']['entries'][$o->id] = $o ;
		if ( $o->q != null ) $q[] = $o->q ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}

	$out['data']['users'] = get_users ( $users ) ;

} else if ( $query == 'catalog_details' ) {

	$catalog = get_request ( 'catalog' , 0 ) * 1 ;
	
	$sql = "select type,count(*) as cnt from entry where catalog=$catalog group by type order by cnt desc" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $out['data']['type'][] = $o ;
	
	$sql = "select substring(timestamp,1,6) AS ym,count(*) as cnt from entry where catalog=$catalog and timestamp is not null and user!=0 group by ym order by ym" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $out['data']['ym'][] = $o ;
	
	$sql = "select tusc_username as username,entry.user AS uid,count(*) as cnt from entry,user where catalog=$catalog and entry.user=user.id and user!=0 and entry.user is not null group by uid order by cnt desc" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $out['data']['user'][] = $o ;

} else if ( $query == 'match_q' ) {

	$entry = $db->real_escape_string ( get_request ( 'entry' , -1 ) * 1 ) ;
	$tusc_project = get_request ( 'tusc_project' , -1 ) ;
//	$q = $db->real_escape_string ( get_request ( 'q' , -1 ) * 1 ) ;
	$q = '' . ( get_request ( 'q' , -1 ) * 1 ) ;

	if ( verify_tusc2() ) {
		$user = $db->real_escape_string ( $tusc_user ) ;
		$project = $db->real_escape_string ( $tusc_project ) ;
		$user_id = ensureUserID ( $user , $project ) ;
		$ts = date ( 'YmdHis' ) ;
		$sql = "UPDATE entry SET q=$q,user=$user_id,timestamp='$ts' WHERE id=$entry" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		
		$cat = '' ;
		$sql = "SELECT * FROM entry,catalog WHERE entry.id=$entry and entry.catalog=catalog.id" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$out['entry'] = $o ;
			$cat = $o->catalog ;
		}
		
		update_overview ( $cat ) ;
	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}

} else if ( $query == 'match_q_multi' ) {

	$data = json_decode ( get_request('data','[]') ) ;
	$tusc_project = get_request ( 'tusc_project' , -1 ) ;
	$catalog = get_request ( 'catalog' , -1 ) * 1 ;
	
	if ( verify_tusc2() ) {
		
		$user = $db->real_escape_string ( $tusc_user ) ;
		$project = $db->real_escape_string ( $tusc_project ) ;
		$user_id = ensureUserID ( $user , $project ) ;
		$ts = date ( 'YmdHis' ) ;
		
		foreach ( $data AS $d ) {
			$q = $d[0] * 1 ;
			$extid = $db->real_escape_string ( $d[1] ) ;
			if ( $q == 0 ) continue ; // Paranoia
			$sql = "UPDATE entry SET q=$q,user=$user_id,timestamp='$ts' WHERE catalog=$catalog AND (user is null or user=0 or q=-1) AND ext_id='$extid'" ;
			if(!$result = $db->query($sql)) $out['status'] = 'There was an error running the query [' . $db->error . ']: ' . $sql ;
//			$out['data'][] = $sql ;
		}

		update_overview ( $catalog ) ;

	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}


} else if ( $query == 'remove_q' ) {

	$entry = $db->real_escape_string ( get_request ( 'entry' , -1 ) * 1 ) ;
	$tusc_project = get_request ( 'tusc_project' , -1 ) ;
	
	if ( verify_tusc2() ) {
		$user = $db->real_escape_string ( $tusc_user ) ;
		$project = $db->real_escape_string ( $tusc_project ) ;
		$user_id = ensureUserID ( $user , $project ) ;
		$ts = date ( 'YmdHis' ) ;
		$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE id=$entry" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		$sql = "INSERT INTO log (action,entry,user,timestamp) VALUES ('remove_q',$entry,$user_id,'$ts')" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');

		$sql = "SELECT * FROM entry WHERE id=$entry" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			update_overview ( $o->catalog ) ;
		}
	} else {
		$out['status'] = "Cannot verify TUSC user $tusc_user on Commons" ;
	}

} else if ( $query == 'rc' ) {

	$limit = 100 ;
	$ts = get_request ( 'ts' , '' ) ;
	$events = array() ;
	
	$sql = "SELECT * FROM entry WHERE user not in (0,3,4) and timestamp IS NOT NULL" ;
	if ( $ts != '' ) $sql .= " AND timestamp >= '" . $db->real_escape_string($ts) . "'" ;
	$sql .= " ORDER BY timestamp DESC LIMIT $limit" ;
	$min_ts = '' ;
	$max_ts = $ts ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$users = array() ;
	while($o = $result->fetch_object()){
		$o->event_type = 'match' ;
		$events[$o->timestamp] = $o ;
		if ( $min_ts == '' ) $min_ts = $o->timestamp ;
		$max_ts = $o->timestamp ;
		$users[$o->user] = 1 ;
	}

	$sql = "SELECT entry.id AS id,catalog,ext_id,ext_url,ext_name,ext_desc,action AS event_type,log.user AS user,log.timestamp AS timestamp FROM log,entry WHERE log.entry=entry.id AND log.timestamp BETWEEN '$max_ts' AND '$min_ts'" ;
	$out['sql'] = $sql ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$events[$o->timestamp] = $o ;
		$users[$o->user] = 1 ;
	}
	
	krsort ( $events ) ;
	
	while ( count ( $events ) > $limit ) array_pop ( $events ) ;
	
	$out['data']['events'] = $events ;
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'search' ) {
	
	$what = get_request ( 'what' , '' ) ;
	$exclude = preg_replace ( '/[^0-9,]/' , '' , get_request ( 'exclude' , '' ) ) ;
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	
	$s = array() ;
	$what2 = explode ( " " , $what ) ;
	foreach ( $what2 AS $w ) {
		$w = $db->real_escape_string ( trim ( $w ) ) ;
		if ( $w == '' ) continue ;
		$s[] = "(ext_name LIKE \"%" . $w . "%\")" ;
	}
	
//	$sql = "SELECT * FROM entry WHERE ext_name LIKE '%" . $db->real_escape_string ( $what ) . "%' LIMIT 25" ;
	$sql = "SELECT * FROM entry WHERE " . implode ( " AND " , $s ) ;
	if ( $exclude != '' ) $sql .= " AND catalog not in ($exclude)" ;
	$sql .= " LIMIT 25" ;
	
	if ( preg_match ( '/^\s*[Qq]{0,1}(\d+)\s*$/' , $what , $m ) ) {
		$sql = "SELECT * FROM entry WHERE q=".$m[1] ;
	}
	
	$out['sql'] = $sql ;
	$db->set_charset('utf8mb4'); 
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}


	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'create' ) {
	
	$out['total'] = array() ;
	$catalog = get_request ( 'catalog' , '' ) ;
	$sql = "select ext_id,ext_name,ext_desc,ext_url,type FROM entry WHERE q=-1 AND user>0 AND catalog=" . $db->real_escape_string ( $catalog ) . " ORDER BY ext_name" ;
	$out['sql'] = $sql ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['data'][] = $o ;
	}

} else if ( $query == 'sitestats' ) {
	
	$out['total'] = array() ;
	$catalog = get_request ( 'catalog' , '' ) ;
	
	$sql = "SELECT distinct catalog,q FROM entry WHERE user>0 and q is not null" ;
	if ( $catalog != '' ) $sql .= " AND catalog=" . $db->real_escape_string ( $catalog ) ;
	$catalogs = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$catalogs[$o->catalog][] = $o->q ;
	}
	
	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $qs ) {
		$qs = implode ( ',' , $qs ) ;
		$sql = "select ips_site_id,count(*) AS cnt from wb_items_per_site where ips_item_id IN ($qs) group by ips_site_id" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
		while($o = $result->fetch_object()){
			$out['data'][$o->ips_site_id][$cat] = $o->cnt ;
		}
	}

	$sql = "select catalog,count(*) AS cnt from entry where q>0 and user>0 group by catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$out['total'][$o->catalog] = $o->cnt ;
	}

} else if ( $query == 'missingpages' ) {

	$catalog = $db->real_escape_string ( get_request ( 'catalog' , 0 ) ) ;
	
	$sql = "SELECT DISTINCT q FROM entry WHERE q>0 AND user>0 AND catalog=$catalog AND q is not null" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $qs[''.$o->q] = $o->q ;

	if ( count($qs) > 0 ) {
		$dbwd = openDB ( 'wikidata' ) ;
		$site = $dbwd->real_escape_string ( get_request ( 'site' , '' ) ) ;
		$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_item_id IN (" . implode(',',$qs) . ") AND ips_site_id='$site'" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
		while($o = $result->fetch_object()) {
			unset ( $qs[''.$o->ips_item_id] ) ;
		}
	}

	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$users = array() ;
	if ( count($qs) > 0 ) {
		$sql = "SELECT * FROM entry WHERE user>0 AND catalog=$catalog AND q IN (" . implode(',',$qs) . ")" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$out['data']['entries'][$o->id] = $o ;
			if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
		}
		$out['data']['users'] = get_users ( $users ) ;
	}


} else if ( $query == 'get_sync' ) {

	$catalog = $db->real_escape_string ( get_request ( 'catalog' , 0 ) * 1 ) ;
	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$prop = $o->wd_prop ;
		$qual = $o->wd_qual ;
	}
	
	if ( !isset($prop) ) $prop = '' ;
	if ( !isset($qual) ) $qual = '' ;
	
	if ( $prop == '' ) {
		$out['status'] = 'No Wikidata property defined for this catalog' ;
		if ( $callback != '' ) print $callback.'(' ;
		print json_encode ( $out ) ;
		if ( $callback != '' ) print ')' ;
		exit ( 0 ) ;
	}
	
	$out['data']['prop'] = $prop ;
	$out['data']['qual'] = $qual ;
	
	// Get Wikidata state
	$dont_compare_values = false ;
	$query = '' ;
	$the_prop = '' ;
	if ( $qual == '' ) {
		$query = "claim[$prop]" ;
		$the_prop = $prop ;
	} else {
		if ( $prop == '958' ) {
			$query = "claim[1343:$qual]{claim"."[$prop]}" ;
			$the_prop = 1343 ;
			$dont_compare_values = true ;
		} else { // 528
			$query = "claim[528]{claim"."[972:$qual]}" ;
			$the_prop = $prop ;
		}
	}
	$out['test'] = array ( 'prop'=>$prop , 'query'=>$query ) ;
	
	
	$url = "$wdq_internal_url?q=" . urlencode ( $query ) ;
	if ( !$dont_compare_values ) $url .= "&props=$the_prop" ; // TODO: For the 1343 case, this should return the qualifier P958
//	print "$url\n" ; exit(0);
//	print_r ( $url ) ;
//	print "<pre>" ; print_r ( $j ) ; print "</pre>" ;

	
	$wd = array() ;
	if ( $dont_compare_values ) {
		$j = json_decode ( file_get_contents ( $url ) ) ;
		foreach ( $j->items AS $q ) $wd[$q][] = '' ;
	} else {
		$j = file_get_contents ( $url ) ;
		
		$off = 0 ;
		while ( preg_match ( '/\[(\d+),"string","(.+?)"\]/' , $j , $m , PREG_OFFSET_CAPTURE , $off ) ) {
//			print_r ( $m ) ;
			$wd[$m[1][0]*1][] = json_decode ( '"'.$m[2][0].'"' ) ;
			$off = $m[2][1] ;
		}
		
//		[584500,"string","101101900"]
		
//		$j = json_decode ( file_get_contents ( $url ) ) ;
//		foreach ( $j->props->$the_prop AS $v ) {
//			$wd[$v[0]*1][] = $v[2] ;
//		}
	}
	
//	print "<pre>" ; print_r ( $wd ) ; print "</pre>" ;

	// Get Mix-n-match state
	$mm = array() ;
	$sql = "select ext_id,q from entry where q is not null and q>0 and user!=0 and user is not null and catalog=$catalog and ext_id not like 'fake_id_%'" ;
//	print_r ( $sql ) ;
	$db = openMixNMatchDB() ;
//	$db->ping() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		if ( $o->q*1 == 0 ) continue ; // Paranoia
		$mm[$o->q*1][] = $o->ext_id ;
	}
	
	if ( $dont_compare_values ) {
		foreach ( $wd AS $q => $a ) {
			if ( isset($mm[$q]) ) $wd[$q] = $mm[$q] ;
			else unset ( $mm[$q] ) ;
		}
	}

//	print "<pre>" ; print_r ( $mm ) ; print "</pre>" ; exit ( 0 ) ;

	// Report
	$out['data']['mm_dupes'] = array() ;
	foreach ( $mm AS $q => $v1 ) {
		if ( count($v1) == 1 ) continue ;
		$b = array() ;
		foreach ( $v1 AS $v2 ) {
			$b[] = utf8_encode($v2);
		}
		$out['data']['mm_dupes'][] = array ( $q*1 , $b ) ;
		$bad_q[$q*1] = 1 ;
	}

	$out['data']['different'] = array() ;
	foreach ( $wd AS $q => $v1 ) {
		if ( !isset ( $mm[$q] ) ) continue ;
		if ( count($v1) == 1 and count($mm[$q]) == 1 and ( $dont_compare_values or $v1[0] == $mm[$q][0] ) ) continue ; // Same value
		sort($v1) ;
		sort($mm[$q]) ;
		if ( implode('|',$v1) == implode('|',$mm[$q]) ) continue ;
		$out['data']['different'][] = array ( $q , utf8_encode($v1) , utf8_encode($mm[$q]) ) ;
	}
#	print json_encode  ( $out ) ; exit ( 0 ) ;

	$out['data']['wd_no_mm'] = compareSets ( $wd , $mm ) ;
	$out['data']['mm_no_wd'] = compareSets ( $mm , $wd ) ;

	$out['data']['mm_double'] = array() ;
	$sql = "select q,count(*) as cnt,group_concat(id) AS ids from entry where q>0 and catalog=$catalog and q is not null and user is not null and user>0 group by q having cnt>1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$out['data']['mm_double'][$o->q] = explode ( ',' , $o->ids ) ;
	}
	

} else if ( $query == 'get_sync_all' ) {

	$catalogs = array() ;
	$sql = "SELECT * FROM catalog WHERE wd_prop IS NOT NULL and wd_qual IS NULL" ;
#$sql .= " AND wd_prop=1415" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$catalogs[$o->id] = $o->wd_prop ;
	}
	
	$human_issue = array() ;
	$deleted = array() ;

	$dbwd = openDB ( 'wikidata' ) ;
	foreach ( $catalogs AS $cat => $prop ) {
		$query = 'claim[' . $prop . ']' ;
		$url = "$wdq_internal_url?q=" . urlencode ( $query ) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		
		$list = array() ;
		$qlist = array() ;
		$sql = "SELECT * FROM entry WHERE catalog=$cat AND q is not null and q>0 and user!=0" ;
		if ( count($j->items) > 0 ) $sql .= " AND q NOT IN (" . implode ( ',' , $j->items ) . ")" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()){
			$list[] = $o ;
			$qlist[] = $o->q ;
		}

		if ( count ( $list ) == 0 ) continue ;
		

		$tmp = array() ;
		$sql = "select epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ")" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()){
			$tmp[''.$o->epp_entity_id] = 1 ;
		}

		$potential_people = array() ;		
		$sql = "select distinct epp_entity_id from wb_entity_per_page where epp_entity_type='item' and epp_entity_id IN (" . implode ( ',' , $qlist ) . ") and not exists (select * from pagelinks where pl_from=epp_page_id and pl_namespace=0 and pl_title='Q5')" ;
		if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n");
		while($o = $result->fetch_object()) $potential_people[] = $o->epp_entity_id ;

		if ( count($potential_people) > 0 ) {
			$sql = "SELECT * from entry WHERE `type`='person' and q in (" . implode(',',$potential_people) . ")" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
			while($o = $result->fetch_object()) $human_issue[''.$o->q] = $o->q ;
		}
		
		foreach ( $qlist AS $q ) {
			if ( !isset ( $tmp[$q] ) ) $deleted[''.$q] = $q ;
		}

		foreach ( $list AS $o ) {
			if ( isset($deleted[$o->q]) ) continue ;
			if ( isset($human_issue[$o->q]) ) continue ;
			print "Q" . $o->q . "\tP$prop\t\"" . $o->ext_id . "\"\n" ;
		}
	}

	foreach ( $human_issue AS $q ) {
		print "HUMAN ISSUE: Q$q\n" ;
	}

	$updated = false ;
	foreach ( $deleted AS $q ) {
		if ( isset ( $_REQUEST['delete'] ) ) {
			$sql = "UPDATE entry SET q=null,user=null,timestamp=null WHERE q=$q" ;
			if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
			$updated = true ;
		} else {
			print "DELETED: Q$q\n" ;
		}
	}

	if ( $updated ) update_overview ( $o->catalog ) ;
	
	exit ( 0 ) ;
	
} else if ( $query == 'random' ) {

	$submode = get_request ( 'submode' , '' ) ;
	$catalog = $db->real_escape_string ( get_request ( 'catalog' , 0 ) * 1 ) ;
	$cnt = 0 ;
	while ( 1 ) {
		$r = rand() / getrandmax() ;
		if ( $cnt > 5 ) $r = 0 ;
		$sql = "SELECT * FROM entry WHERE random>=$r " ;
		if ( $submode == 'prematched' ) $sql .= " AND user=0" ;
		else $sql .= " AND user IS NULL" ; // Default: unmatched
		if ( $catalog > 0 ) $sql .= " AND catalog=$catalog" ;
		$sql .= " ORDER BY random limit 1" ;
if ( isset($_REQUEST['id']) ) $sql = "SELECT * FROM entry WHERE id=" . ($_REQUEST['id']*1) ; // FOR TESTING
		$out['sql'] = $sql ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()) {
			$out['data'] = $o ;
		}
		if ( count ( $out['data'] ) > 0 ) break ;
		$cnt++ ;
	}

} else if ( $query == 'same_names' ) {

	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$users = array() ;
	$sql = "select ext_name,count(*) as cnt,SUM(if(q is not null or q=0, 1, 0)) AS matched from entry " ;
	if ( rand(0,10) > 5 ) $sql .= " where ext_name>'M' " ; // Hack to get more results
	$sql .= " group by ext_name having cnt>1 and cnt<10 and matched>0 and matched<cnt limit 10000" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$tmp = array() ;
	while($o = $result->fetch_object()) {
		$tmp[] = $o ;
	}
	
	$ext_name = $tmp[array_rand($tmp)]->ext_name ;
	$out['data']['name'] = $ext_name ;
	
	$sql = "SELECT * FROM entry WHERE ext_name='" . $db->real_escape_string($ext_name) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'creation_candidates' ) {

	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$users = array() ;
	$sql = "select name AS ext_name,cnt from common_names order by rand() limit 1" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$tmp = array() ;
	while($o = $result->fetch_object()) {
		$tmp[] = $o ;
	}
	
	$ext_name = $tmp[array_rand($tmp)]->ext_name ;
	$out['data']['name'] = $ext_name ;
	
	$sql = "SELECT * FROM entry WHERE ext_name='" . $db->real_escape_string($ext_name) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;


} else if ( $query == 'disambig' ) {

	$catalog = get_request ( 'catalog' , 0 ) ;
	if ( $catalog != '' ) $catalog *= 1 ;
	
	$qs = '' ;
	$sql = "SELECT DISTINCT q FROM entry WHERE q IS NOT NULL and q>0 and user!=0" ;
	if ( $catalog != '' ) $sql .= " AND catalog=$catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		if ( $qs != '' ) $qs .= "," ;
		$qs .= $o->q ;
	}
	$out['data']['qs'] = count($qs) ;
	
	$out['data']['entries'] = array() ;
	$out['data']['users'] = array() ;
	$sql = "SELECT DISTINCT epp_entity_id FROM wb_entity_per_page,pagelinks WHERE epp_entity_type='item' and epp_entity_id IN ($qs) and pl_from=epp_page_id AND pl_namespace=0 AND pl_title='Q4167410' ORDER BY rand() LIMIT 50" ;

	$qs = array() ;
	$dbwd = openDB ( 'wikidata' ) ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	while($o = $result->fetch_object()) $qs[] = $o->epp_entity_id ;
	$dbwd->close() ;
	
	$users = array() ;
	$sql = "SELECT * FROM entry WHERE q IN (" . implode(',',$qs) . ") and user!=0" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		$out['data']['entries'][] = $o ;
		if ( isset ( $o->user ) ) $users[$o->user] = 1 ;
	}
	$out['data']['users'] = get_users ( $users ) ;

} else {
	$out['status'] = 'Unknown action "' . $query . '"' ;
}


if ( $callback != '' ) print $callback.'(' ;
print json_encode ( $out ) ;
if ( $callback != '' ) print ')' ;
myflush();

?>