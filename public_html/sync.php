<?PHP

require_once ( 'php/common.php' ) ;
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

$diff = array ( 'wd_no_mm' => array() , 'mm_no_wd' => array() ) ;

function compareSets ( $s1 , $s2 , $key ) {
	global $diff ;
	foreach ( $s1 AS $q => $v1 ) {
		foreach ( $v1 AS $extid ) {
			if ( !isset ( $s2[$q] ) ) {
				$diff[$key][] = array ( $q , $extid ) ;
				continue ;
			}
			if ( !in_array ( $extid , $s2[$q] ) ) {
				$diff[$key][] = array ( $q , $extid ) ;
				continue ;
			}
		}
	}
}

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;

//$prop = get_request ( 'prop' , '' ) ;
//$qual = get_request ( 'qual' , '' ) ;
$catalog = get_request ( 'catalog' , '' ) ;

// Sanitize
//$prop = preg_replace ( '/\D/' , '' , $prop ) ;
//$qual = preg_replace ( '/\D/' , '' , $qual ) ;
$catalog = preg_replace ( '/\D/' , '' , $catalog ) ;

// Output
print get_common_header ( '' , 'Sync to Wikidata' ) ;

if ( $catalog == '' ) {
	print "
<form method='get' class='form inline-form'>
<div class='input-group input-group-lg'><span class='input-group-addon'>Catalog</span><input type='text' name='catalog' class='form-control' placeholder=\"Mix'n'match catalog number\" value='$catalog' /></div>
<input type='submit' value='Check sync status' class='btn btn-primary' />
</form>" ;

/*
<div class='input-group input-group-lg'><span class='input-group-addon'>Property</span><input type='text' name='prop' class='form-control' placeholder='Pxxx' value='$prop' /></div>
<div class='input-group input-group-lg'><span class='input-group-addon'>Qualifier</span><input type='text' name='qual' class='form-control' placeholder='Qxxx; qualifier item, for P528/P972' value='$qual' /></div>
*/


} else {

	$sql = "SELECT * FROM catalog WHERE id=$catalog" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$prop = $o->wd_prop ;
		$qual = $o->wd_qual ;
	}
	
	// Get Wikidata state
	$query = "claim[$prop]" ;
	$the_prop = $prop ;
	if ( $qual != '' ) {
		$query = "claim"."[$prop]{claim"."[972:$qual]}" ;
	}
	$url = "$wdq_internal_url?q=" . urlencode ( $query ) . "&props=$the_prop" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
//	print_r ( $url ) ;
//	print "<pre>" ; print_r ( $j ) ; print "</pre>" ;
	
	$wd = array() ;
	foreach ( $j->props->$the_prop AS $v ) {
		$wd[$v[0]*1][] = $v[2]*1 ;
	}
	
//	print "<pre>" ; print_r ( $wd ) ; print "</pre>" ;

	// Get Mix-n-match state
	$mm = array() ;
	$sql = "select ext_id,q from entry where q is not null and user!=0 and user is not null and catalog=$catalog" ;
//	print_r ( $sql ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$mm[$o->q*1][] = $o->ext_id*1 ;
	}

//	print "<pre>" ; print_r ( $mm ) ; print "</pre>" ;

	// Report
	compareSets ( $wd , $mm , 'wd_no_mm' ) ;
	compareSets ( $mm , $wd , 'mm_no_wd' ) ;
	
	print "<pre>" ; print_r ( $diff ) ; print "</pre>" ;

}

print get_common_footer() ;

?>