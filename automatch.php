#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);

if ( !isset ( $argv[1] ) ) {
	print "Needs argument : catalog_id\n" ;
	exit ( 0 ) ;
}

$catalog = $argv[1] ;
$en_only = false ;

require_once ( 'public_html/php/common.php' ) ;
require_once ( 'opendb.inc' ) ; // $db = openMixNMatchDB() ;

function getSearch ( $query ) {
	return json_decode ( file_get_contents ( "http://www.wikidata.org/w/api.php?action=query&list=search&format=json&srsearch=" . urlencode ( $query ) ) ) ;
}

$db = openMixNMatchDB() ;

$name2ids = array() ;
$sql = "SELECT id,lower(ext_name) AS name FROM entry WHERE catalog=$catalog and q IS NULL" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$name2ids[$o->name][] = $o->id ;
}

$dbwd = openDB ( 'wikidata' ) ;
$names = array() ;
foreach ( $name2ids AS $k => $v ) {
	$names[] = $dbwd->real_escape_string ( $k ) ;
}
if ( count($names) == 0 ) exit(0) ;


while ( count($names) > 0 ) {
	$names2 = array() ;
	while ( count($names) > 0 and count($names2) < 100000 ) $names2[] = array_pop ( $names ) ;
	$sql = "SELECT term_search_key AS name,term_entity_id AS q,count(DISTINCT term_entity_id) AS cnt FROM wb_terms AS terms WHERE " ;
	$sql .= " term_type IN ('label','alias') and term_search_key IN ('" . implode("','",$names2) . "') and term_entity_type='item'" ;
	if ( $en_only ) $sql .= " and term_language='en' " ;
	$sql .= " group by name HAVING cnt=1" ;
	$dbwd = openDB ( 'wikidata' ) ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']');
	while($o = $result->fetch_object()){
		if ( !isset($name2ids[$o->name]) ) continue ;
		foreach ( $name2ids[$o->name] AS $id ) {
			$candidates[''.$id] = $o->q ;
		}
	}
}


$db = openMixNMatchDB() ;

$ts = date ( 'YmdHis' ) ;
foreach ( $candidates AS $entry => $q ) {
	$sql = "UPDATE entry SET q=$q,user=0,timestamp='$ts' WHERE id=$entry AND q IS NULL" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
}



file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=$catalog" ) ; // Update stats

?>
