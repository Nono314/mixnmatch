#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 72 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

foreach my $letter ( 'A' .. 'Z' ) {
	#batch_parse () ;
	continuous_parse ( 'https://www.eerstekamer.nl/oud_leden?start=$1&s01=jv5igwrrffhe1h&_charset_=UTF-8&u1a='.$letter.'&dlgid=jv5igws1zythc' , 0 , 50 ) ;
}

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub parse_page {
	my ( $page ) = @_ ;
	my $ret = 0 ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<li><span>\s*<a href="/persoon/(.+?)" data-title="" data-tooltip="">(.+?)</a>\s*<br /><span class="bijschrift">(.+?)</span>\s*</span></li>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "https://www.eerstekamer.nl/persoon/$1" ;
		$out->{name} = $2 ;
		$out->{desc} = $3 ;
		
		if ( $out->{name} =~ m|^(.+) \((.+)\)$| ) {
			$out->{desc} .= " $2" ;
			$out->{name} = $1 ;
		}
		
		$out->{name} =~ s|^([^,]+), ([^,]+)$|$2 $1| ;
		
		if ( $out->{name} =~ m|(\S+) (\S+)$| ) {
			if ( $1 eq $2 ) {
				$out->{name} =~ s|(\S+)$|| ;
			}
		}
		
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$ret++ ;
	}
	return $ret ;
}

