#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;
print OUT '{"catalog":46,"entries":' ;
print OUT "[\n" ;

my @urls = (
	'http://www.assemblee-nationale.fr/sycomore/result.asp?radio_dept=tous_departements&regle_nom=est&Nom=&departement=&choixdate=intervalle&D%C3%A9butMin=&FinMin=&Dateau=&legislature=&choixordre=chrono&Rechercher=Lancer+la+recherche' ,
	'http://www.assemblee-nationale.fr/sycomore/_result.asp?radio_dept=etats_generaux&regle_nom=est&Nom=&choixdate=intervalle&D%C3%A9butMin=&FinMin=&Dateau=&choixordre=chrono&Rechercher=Lancer+la+recherche' 
) ;

my %name_cache ;
my $first = 1 ;
foreach my $url ( @urls ) {
	my $main = get $url ;
	$main =~ s/\s+/ /g ;

	my $last_id = '' ;
	while ( $main =~ m|<tr><td><a href="_*fiche\.asp\?num_dept=(\d+)" >(.+?)</a></td><td >(.*?)</td><td >(.*?)</td></tr>|g ) {
		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://www.assemblee-nationale.fr/sycomore/fiche.asp?num_dept=$1" ;
		$out->{name} = $2 ;
		if ( $4 eq '' ) {
			$out->{desc} = "born $3" ;
		} else {
			$out->{desc} = "$3 - $4" ;
		}
		$out->{desc} =~ s|(\d{2})/(\d{2})/(\d{4})|$3-$2-$1|g ;
	
		$out->{name} =~ s/&nbsp;/ /g ;
		if ( $out->{name} =~ m/^(.+?) ([A-ZÉÈ -]+)$/ ) {
			my ( $first , $last ) = ( $1 , lc $2 ) ;
			my @a ;
		
			@a = split '-' , $last ;
			$a[$_] = ucfirst ( $a[$_] ) foreach ( 0 .. $#a ) ;
			$last = join '-' , @a ;

			@a = split ' ' , $last ;
			$a[$_] = ucfirst ( $a[$_] ) foreach ( 0 .. $#a ) ;
			$last = join ' ' , @a ;
		
			$out->{name} = "$first $last" ;
		}
	
	
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}
print OUT "\n]}" ;

0 ;


sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}