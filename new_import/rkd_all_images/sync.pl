#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 96 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

continuous_parse ( 'https://api.rkd.nl/api/search/images?fieldset=brief&callback=jQuery19106780119436284712_1442235569676&query=&start=$1&usedefaultfacets=0&rows=50&params%5Bdetail_url%5D=%252Fen%252Fexplore%252Fimages%252Frecord&language=en-gb' , 0 , 50 ) ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'painting' } ;
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########


sub parse_page {
	my ( $page , $url ) = @_ ;
	
	$page =~ s/^[^(]+\(// ;
	$page =~ s/\);$// ;
	my $j = decode_json ( $page ) ;
	return 0 unless defined $j->{response} ;
	return 0 unless defined $j->{response}->{docs} ;
	
	my $cnt = 0 ;
	
	foreach my $i ( @{$j->{response}->{docs}} ) {
		my $out = get_blank() ;
		
		$out->{id} = $i->{id} ;
		$out->{url} = "https://rkd.nl" . $i->{detail_url} ;
		
		if ( defined $i->{title_engels} ) {
			$out->{name} = $i->{title_engels} ;
		} else {
			$out->{name} = $i->{benaming_kunstwerk}->[0] ;
		}
		
		my @d ;
		if ( defined $i->{toeschrijving} ) {
			my $desc = $i->{toeschrijving}->[0] ;
			if ( defined $desc->{naam_engels} ) {
				push @d , $desc->{naam} || '' ;
			} else {
				push @d , $desc->{naam_engels} || '' ;
			}
			
		}
		push @d , join $i->{datering} if defined $i->{datering} ;
		
		$out->{desc} = join "; " , @d ;
		


		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}
	
	return $cnt ;
}

