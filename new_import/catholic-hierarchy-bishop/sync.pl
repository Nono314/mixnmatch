#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $main = get 'http://www.catholic-hierarchy.org/bishop/la.html' ;
$main =~ s/\s+/ /g ;

my $last_id = '' ;
my $first = 1 ;
my %name_cache ;
print OUT '{"catalog":43,"entries":' ;
print OUT "[\n" ;
while ( $main =~ m|<a href="(la.\d*.html)">|g ) {
	my $url = "http://www.catholic-hierarchy.org/bishop/$1" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	foreach my $r ( @rows ) {

		if ( $r =~ m|^\s*<li><a href="(b[^/]+)\.html">(.+?)</a>(.*?)$| ) {
			my $out = get_blank() ;
			$out->{id} = $1 ;
			$out->{url} = "http://www.catholic-hierarchy.org/bishop/$1.html" ;
			$out->{name} = $2 ;
			$out->{desc} = $3 ;
			
			$out->{id} =~ s/^b// ; # WIkidata does not use the leading "b" for bishop

			while ( $out->{name} =~ m/\b(Bishop|Archbishop|Cardinal)\b/g ) {
				$out->{desc} .= "; $1" ;
			}

			$out->{name} =~ s|<.+?>||g ;
			$out->{name} =~ s/\b(Bishop|Archbishop|Cardinal)\b//g ;
			$out->{name} =~ s|<.+?>||g ;
			$out->{name} =~ s|\s+| |g ;
			$out->{name} =~ s|^\s+||g ;

			$out->{desc} =~ s|<.+?>||g ;
			$out->{desc} =~ s|\s+| |g ;
			$out->{desc} =~ s|^\s+||g ;
			
			if ( defined $name_cache{$out->{name}} ) {
				$out->{name} .= " (" . $out->{id} . ")" ;
			}
			next if $out->{name} eq 'Vacant' ;
			
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
		}
	}
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}