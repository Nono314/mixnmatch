#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

# Set by Federico Leva / federicoleva@tiscali.it
my %id2itwiki ;
=cut
open IN , "DBI.tsv" ;
while ( <IN> ) {
	next unless $_ =~ m|^(\S+)\shttp\S+\s(.+)$| ;
	$id2itwiki{$1} = $2 ;
}
close IN ;
=cut


open OUT, "> out.json" or die $! ;

print OUT '{"catalog":55,"entries":' ;
print OUT "[\n" ;
my $first = 1 ;
foreach my $letter ( 'a' .. 'z' ) {
	my $url = "http://www.treccani.it/biografico/elenco_voci/$letter" ;
	my $html = get $url ;
	while ( $html =~ m|<a href="/enciclopedia/(.+?)_\(Dizionario_Biografico\)/" title=".+?">(.+?)</a>|g ) {
		my $o = get_blank () ;
		$o->{id} = $1 ;
		$o->{name} = $2 ;
		$o->{url} = "http://www.treccani.it/enciclopedia/" . $o->{id} . "_(Dizionario_Biografico)/" ;
		if ( $o->{name} =~ m/^([A-Z]+), (.+)$/ ) {
			$o->{name} = $2 . ' ' . ucfirst ( lc $1 ) ;
		}
		
		if ( defined $id2itwiki{$o->{id}} ) {
			push @{$o->{aux}} , { 'catalog' => 'itwiki' , 'id' => $id2itwiki{$o->{id}} } ;
		}
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $o ) ;
	}
}

print OUT "\n]}" ;

0 ;


sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}