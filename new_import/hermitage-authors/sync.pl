#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;
print OUT '{"catalog":64,"entries":' ;
print OUT "[\n" ;

my $index = get 'http://www.arthermitage.org/Author.html' ;
$index =~ s/\s+/ /g ;

my $first = 1 ;
while ( $index =~ m|<a href="/([^"]+?)/index.html">([^<]+?)</a>|g ) {
	my $out = get_blank() ;
	$out->{id} = $1 ;
	$out->{name} = $2 ;
	$out->{url} = "http://www.arthermitage.org/$1/index.html" ;

	$out->{name} =~ s|^(.+), (.+)$|$2 $1| ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}