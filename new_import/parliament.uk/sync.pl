#!/usr/bin/perl

use strict ;
use warnings ;
#use utf8 ;
use LWP::Simple;
use LWP::UserAgent;
use Data::Dumper ;
use JSON ;
use Encode;

my $catalog_id = 97 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

batch_parse () ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

##########

sub generate_pagelist {
	my @pages ;
	push @pages , 'http://www.parliament.uk/mps-lords-and-offices/mps/' ;
	push @pages , 'http://www.parliament.uk/mps-lords-and-offices/lords/' ;
	push @pages , 'http://www.parliament.uk/mps-lords-and-offices/lords/-ineligible-lords/' ;
	return \@pages ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<tr[^>]*>\s*<td[^>]*>\s*<a.+?href="(http://www\.parliament\.uk/biographies/)([^"]+)">(.+?)</a>(.*?)</tr>|g ) {
		my $out = get_blank() ;
		$out->{id} = $2 ;
		$out->{url} = "$1$2" ;
		$out->{name} = $3 ;
		$out->{desc} = $4 ;
		
		$out->{desc} =~ s/<\/.+?>/; /g ;
		$out->{desc} =~ s/<.+?>/ /g ;
		$out->{desc} =~ s/\s+// ;
		$out->{desc} =~ s/^[; ]+// ;
		$out->{desc} =~ s/\s$// ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

