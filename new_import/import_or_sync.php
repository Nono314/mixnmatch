#!/usr/bin/php
<?PHP

require_once ( '../public_html/php/common.php' ) ;
require_once ( '../opendb.inc' ) ;

$show_missing_details = 0 ;
$simulate = 0 ;
if ( isset($argv[2]) and $argv[2] == 'sim' ) $simulate = 1 ;

if ( !isset($argv[1]) ) die ( "Pass JSON file as first parameter!\n" ) ;
$file = $argv[1] ;
$j = json_decode ( file_get_contents ( $file ) ) ;
$catalog = $j->catalog * 1 ;
$new_entries = 0 ;

$db = openMixNMatchDB() ;
$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$db->set_charset("utf8") ;

function alterDB ( $sql ) {
	global $db , $simulate ;
	if ( $simulate ) {
		print "SIM: $sql\n" ;
		return ;
	}
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

function entries_not_in_master () {
	global $j , $catalog , $db , $dbwd , $show_missing_details ;
	$entries = array() ;
	$ids = array() ;
	foreach ( $j->entries AS $e ) $ids[] = "'" . $db->real_escape_string ( $e->id ) . "'" ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND ext_id NOT IN (" . implode(',',$ids) . ")" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$entries[$o->ext_id] = $o ;
	}
	if ( count($entries) == 0 ) return ;
	print count($entries) . " entries not in current master:\n" ;
	if ( $show_missing_details ) { # Show entries not in master
		foreach ( $entries AS $e ) {
			print $e->ext_id . "\t" ;
			print $e->ext_name . "\t" ;
			print $e->ext_url . "\n" ;
		}
	}
	print "\n-----\n\n" ;
}

function get_or_create_entry ( $e ) {
	global $j , $catalog , $db , $dbwd , $simulate , $new_entries ;
	$sql = "SELECT * FROM entry WHERE catalog=$catalog AND ext_id='" . $db->real_escape_string ( $e->id ) . "'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		return $o ;
	}
#	print $e->id . " does not exist in catalog $catalog, creating\n" ;
	foreach ( array('url','name','desc','type') AS $k ) {
		if ( !isset($e->$k) ) $e->$k = '' ;
	}
	$sql = "INSERT INTO entry (catalog,ext_id,ext_url,ext_name,ext_desc,type,random) VALUES ($catalog" ;
	$sql .= ",'" . $db->real_escape_string ( $e->id ) . "'" ;
	$sql .= ",'" . $db->real_escape_string ( $e->url ) . "'" ;
	$sql .= ",'" . $db->real_escape_string ( $e->name ) . "'" ;
	$sql .= ",'" . $db->real_escape_string ( $e->desc ) . "'" ;
	$sql .= ",'" . $db->real_escape_string ( $e->type ) . "'" ;
	$sql .= ",rand())" ;
	$new_entries++ ;
	alterDB ( $sql ) ;
	if ( $simulate ) return ;
	return get_or_create_entry ( $e ) ;
}

function set_q_auto ( &$o , $q ) {
	global $j , $catalog , $db , $dbwd ;
	$ts = date ( 'YmdHis' ) ;
	$sql = "UPDATE entry SET q=$q,user=0,timestamp='$ts' WHERE q IS NULL AND id=" . $o->id ;
#	print "$sql\n" ;
	$o->q = $q ;
	alterDB ( $sql ) ;
}

function set_q_from_wiki_page ( $o , $a ) {
	global $j , $catalog , $db , $dbwd ;

	// Resolve redirect, if any
	$title = urldecode ( str_replace('_',' ',$a->id) ) ;
	if ( preg_match('/[\#\:]/',$title) ) return ;
	$db2 = openDBwiki ( $a->catalog ) ;
	$sql = "select pl_title from page,pagelinks WHERE pl_from=page_id and page_namespace=0 and page_is_redirect=1 and pl_namespace=0 and page_title='" . $db->real_escape_string ( str_replace(' ','_',$title) ) . "' limit 1" ;
	if(!$result = $db2->query($sql)) die('There was an error running the query [' . $db2->error . ']'."\n$sql\n\n");
	while($o2 = $result->fetch_object()) $title = urldecode(str_replace('_',' ',$o2->pl_title)) ;
	$db2->close() ;
	if ( preg_match('/[\#\:]/',$title) ) return ;

	// Find Q
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='" . $db->real_escape_string ( $a->catalog ) . "' AND ips_site_page='" . $db->real_escape_string ( $title ) . "'" ;
	if(!$result = $dbwd->query($sql)) die('There was an error running the query [' . $dbwd->error . ']'."\n$sql\n\n");
	$q = '' ;
	while($o2 = $result->fetch_object()) $q = $o2->ips_item_id ;
	if ( $q == '' ) { // Not found
		print "Could not find Q number for " . $a->catalog . ":" . $title . "\n" ;
		return ;
	}

	// Update match
	set_q_auto ( $o , $q ) ;
}

function set_aux_prop ( $o , $a , $aux ) {
	global $j , $catalog , $db , $dbwd , $wdq_internal_url ;

	// Check if aux already exists
	$prop = preg_replace('/\D/','',$a->prop) ;
	$val = $a->id ;
	
	// Format hacks
	if ( $prop == '1415' ) {
		while ( strlen($val) < 6 ) $val = "0$val" ;
		if ( strlen($val) == 6 ) $val = "101$val" ;
	}
	
	$exists = false ;
	foreach ( $aux AS $a2 ) {
		if ( $a2->aux_p != $prop ) continue ;
		if ( $a2->aux_name != $val ) continue ;
		$exists = true ;
		break ;
	}
	if ( $exists ) return ;
	
	// Create new aux
	$n = (object) array() ;
	$n->aux_p = $prop ;
	$n->aux_name = $val ;
	$aux[] = $n ;
	$sql = "INSERT IGNORE INTO auxiliary (entry_id,aux_p,aux_name) VALUES (" . $o->id . ",$prop,'" . $db->real_escape_string ( $val ) . "')" ;
	alterDB ( $sql ) ;
	
	// Set from WDQ
	if ( $o->q == '' and $prop != 21 and $prop != 625 ) {
		$wdq = "$wdq_internal_url?q=".urlencode("string[$prop:\"$val\"]") ;
		$j2 = json_decode ( file_get_contents ( $wdq ) ) ;
		if ( count($j2->items) != 1 ) return ;
		set_q_auto ( $o , $j2->items[0] ) ;
	}
}

function update_aux ( $e , $o ) { // This only adds aux data, does not remove it!
	global $j , $catalog , $db , $dbwd ;
	if ( !isset($e->aux) ) return ;
	if ( count($e->aux) == 0 ) return ;
	
	// Load existing auxiliary data
	$aux = array () ;
	$sql = "SELECT * FROM auxiliary WHERE entry_id=" . $o->id ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o2 = $result->fetch_object()) $aux[] = $o2 ;
	
	foreach ( $e->aux AS $a ) {
		if ( isset ( $a->catalog ) ) {
		
			// Set q from Wikipedia
			if ( preg_match ( '/^.+wiki/' , $a->catalog ) and $o->q == '' ) {
				set_q_from_wiki_page ( $o , $a ) ;
			}
			
		} else if ( isset ( $a->prop ) ) {
			set_aux_prop ( $o , $a , $aux ) ;
		} else {
			print "There's something odd about aux data:\n" ;
			print_r ( $e ) ; print "\n" ;
			print_r ( $a ) ; print "\n" ;
		}
	}
}

function import_or_update () {
	global $j , $catalog , $db , $dbwd , $simulate , $new_entries ;
	$cnt = 0 ;
	foreach ( $j->entries AS $e ) {
		if ( strlen ( $e->id ) > 90 ) continue ;
		$cnt++ ;
		$o = get_or_create_entry ( $e ) ;
		if ( !isset($o) ) {
			if ( !$simulate ) print "Tried to create " . $e->id . " but couldn't.\n" ;
			continue ;
		}
		// TODO check values
		update_aux ( $e , $o ) ;
	}
	print "Processed $cnt entries. Added $new_entries new entries.\n" ;
}

entries_not_in_master() ;
import_or_update() ;

file_get_contents ( "https://tools.wmflabs.org/mix-n-match/api.php?query=update_overview&catalog=$catalog" ) ; // Update stats

?>