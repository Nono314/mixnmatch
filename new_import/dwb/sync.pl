#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":25,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'A' .. 'Z' ) {
	my $url = "http://yba.llgc.org.uk/en/$letter/list.html" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $out = get_blank() ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m|^<a href="/en/(.+?).html"> <b>(.+?)</b>(.*?)</a>(.*)$| ) {
			$out->{id} = $1 ;
			$out->{url} = "http://yba.llgc.org.uk/en/$1.html" ;
			$out->{name} = "$3 $2" ;
			push @{$out->{desc}} , $4 ;
			$r = "</p>" if $r =~ m|</p>| ;
		}
		if ( defined $out->{id} and $r =~ m|^\s+(.+)$| ) {
			push @{$out->{desc}} , $1 ;
		}
	
		if ( $r =~ m|^(.*)</p>| ) {
#			push @{$out->{desc}} , $1 ;
			if ( defined $out->{id} ) {

				$out->{name} =~ s/\(\)//g ;
				$out->{name} =~ s/^[ ,]+//g ;
				$out->{name} =~ s/\s+/ /g ;
				$out->{name} = lc $out->{name} ;
				$out->{name} =~ s/\b(\w)/\u$1/g;

				$out->{desc} = join " " , @{$out->{desc}} ;
				$out->{desc} =~ s/<.+?>//g ;
				$out->{desc} =~ s/\s+/ /g ;
				$out->{desc} =~ s/^\s+// ;
				$out->{desc} =~ s/[ .]+$// ;
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$out = get_blank() ;
		}
		
	}
}

sub get_blank {
	return { aux => [] , desc => [] , type => 'person' } ;
}