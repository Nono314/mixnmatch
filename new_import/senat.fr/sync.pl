#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;
print OUT '{"catalog":44,"entries":' ;
print OUT "[\n" ;

my $last_id = '' ;
my $first = 1 ;

my @pages = (
	'http://www.senat.fr/senateurs/senatl.html' ,
	'http://www.senat.fr/anciens-senateurs-5eme-republique/senatl.html' ,
	'http://www.senat.fr/senateurs-4eme-republique/senatl.html' ,
	'http://www.senat.fr/senateurs-3eme-republique/senatl.html'
) ;

foreach my $main_page ( @pages ) {
	my $main = get $main_page ;
	parse_page ( $main ) ;
}



print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	foreach my $r ( @rows ) {

		if ( $r =~ m|^\s*<li><A href="/(senateur.*/.+?)\.html">(.+?)</A></li>$| ) {
			my $out = get_blank() ;
			$out->{id} = $1 ;
			$out->{url} = "http://www.senat.fr/$1.html" ;
			$out->{name} = $2 ;
			$out->{desc} = '' ;

			if ( $out->{name} =~ m|^(.+?)&nbsp;(.+)$| ) {
				$out->{name} = $2 . " " . ucfirst ( lc $1 ) ;
			}
			
			if ( $first ) { $first = 0 ; }
			else { print OUT ",\n" ; }
			print OUT encode_json ( $out ) ;
		}
	}
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}