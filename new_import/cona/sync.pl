#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;
print OUT '{"catalog":50,"entries":' ;
print OUT "[\n" ;

my @alphas = ( 'A' .. 'Z' , '0' .. '9' ) ;
my $first = 1 ;
foreach my $prefix ( '700000001' , '700000002' ) {
	foreach my $alpha ( @alphas ) {
		my $page = 1 ;
		while ( 1 ) {
			my $url = "https://www.getty.edu/cona/CONAHierarchy.aspx?english=N&subid=$prefix&alpha=$alpha&page=$page" ;
			print "$url\n" ;
			my $html = get $url ;
			last if 0 == parse_page ( $html ) ;
			$page++ ;
		}
	}
}

print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my $cnt = 0 ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<td><a class="page" href="CONAFullSubject\.aspx\?subid=(\d+)".+?<b>(.+?)</b>.+?</tr><tr>.*?<td class="page">(.*?)</td>|g ) {
		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{name} = $2 ;
		$out->{desc} = $3 ;
		$out->{url} = "https://www.getty.edu/cona/CONAFullSubject.aspx?subid=$1" ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}
	return $cnt ;
}

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}