#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
print OUT '{"catalog":36,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'A' .. 'Z' ) {
	foreach my $num ( 0 .. 3 ) {
		my $url = "http://resources.huygens.knaw.nl/vrouwenlexicon/voltooide_lemmata/$letter?start=" . ($num * 50 ) ;
		my $page = get $url ;
		parse_page ( $page ) ;
	}
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $is_redirect = 0 ;
	foreach my $r ( @rows ) {
	
		if ( $r =~ m| zie :<span| ) {
			$is_redirect = 1 ;
			next ;
		}

		unless ( $r =~ m|<a href="(http://resources.huygens.knaw.nl/vrouwenlexicon/lemmata/data/)(.+?)">(.+?) \((.+?)\)</a> | ) {
			$is_redirect = 0 ;
			next ;
		}
		
		next if $is_redirect ;
		
		my $out = {
			"id" => $2 ,
			"name" => $3 ,
			"desc" => $4 ,
			"url" => "$1$2" ,
			"type" => "person" ,
			"aux" => []
		} ;
		$out->{name} =~ s/^(.+),\s+([^,]+)$/$2 $1/ ;
		$is_redirect = 0 ;
		
		return if $last_id eq $out->{id} ;
		$last_id = $out->{id} ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}
