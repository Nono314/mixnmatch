#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 93 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

open FILE , 'data.tab' ;
my $h = <FILE> ;
chomp $h ;
my @h = split "\t" , $h ;
my %h ;
foreach ( 0 .. $#h ) {
	$h{$h[$_]} = $_ ;
}
while ( <FILE> ) {
	chomp ;
	next if $_ eq '' ;
	my $line = $_ ;
	utf8::decode ( $line ) ;
	my @d = split "\t" , $line ;
	if ( $#d != $#h ) {
		print "Bad row $line\n" ;
		last ;
	}
	
	my $o = get_blank() ;
	$o->{id} = $d[$h{'World Heritage Site id'}] ;
	$o->{name} = $d[$h{name_en}] ;
	$o->{desc} = $d[$h{short_description_en}] ;
	
	$o->{name} =~ s/<.+?>//g ;
	
	$o->{desc} =~ s/<.+?>//g ;
	$o->{desc} =~ s/\t/ /g ;
	$o->{desc} =~ s/^"//g ;
	$o->{desc} =~ s/"$//g ;
	
	$o->{desc} = substr $o->{desc} , 0 , 150 ;
	$o->{desc} =~ s/\s\S+$// ;
	$o->{desc} .= ' ' ;
	
	
	$o->{desc} .= "|coord="  . $d[$h{latitude}] . "," . $d[$h{longitude}] ;
	$o->{desc} .= "|area_hectares=" . $d[$h{area_hectares}] ;
	$o->{desc} .= "|category=" . $d[$h{category}] ;
	$o->{desc} .= "|states_name_en=" . $d[$h{states_name_en}] ;
	$o->{desc} .= "|" ;

	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $o ) ;
}
close FILE ;


print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'location' } ;
}
