#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my $catalog_id = 84 ;

my $first = 1 ;
open OUT, "> out.json" or die $! ;
print OUT '{"catalog":' . $catalog_id . ',"entries":' ;
print OUT "[\n" ;

batch_parse () ;

print OUT "\n]}" ;

0 ;

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}

sub batch_parse {
	my $index_page_list = generate_pagelist () ;
	foreach my $url ( @{$index_page_list} ) {
		my $page = get $url ;
		parse_page ( $page , $url ) ;
	}
}

sub continuous_parse {
	my ( $url_pattern , $start , $step ) = @_ ;
	my $pos = $start ;
	while ( 1 ) {
		my $url = $url_pattern ;
		$url =~ s/\$1/$pos/g ;
		my $page = get $url ;
		my $found = parse_page ( $page ) ;
		last if $found == 0 ;
		$pos += $step ;
	}
}

##########

sub generate_pagelist {
	my @letters = ( 'a' .. 'z' ) ;
	my @pages ;
	foreach my $letter ( @letters ) {
		push @pages , "http://hansard.millbanksystems.com/people/$letter" ;
	}
	return \@pages ;
}

sub parse_page {
	my ( $page , $url ) = @_ ;
	return unless defined $page ;
	$page =~ s/\s+/ /g ;
	while ( $page =~ m|<li class='person'>\s*<a href="(.+?)">(.+?)</a>\s*<span class='note' title='.*?'>(.*?)</span>\s*</li>|g ) {

		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "http://hansard.millbanksystems.com/people/$1" ;
		$out->{name} = $2 ;
		$out->{desc} = $3 ;
		
		if ( $out->{name} =~ m|\s*\((.+)\)$| ) {
			$out->{desc} .= " ($1)" ;
			$out->{name} =~ s/\s*\(.+\)$// ;
		}
		$out->{name} =~ s/^([^,]+), (.+)$/$2 $1/ ;

		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
	}
}

