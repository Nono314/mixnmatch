#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

open OUT, "> out.json" or die $! ;
my $first = 1 ;
print OUT '{"catalog":60,"entries":' ;
print OUT "[\n" ;

my $url = "http://www.newulsterbiography.co.uk/index.php/home/browse/all" ;
my $page = get $url ;
$page =~ s/\s+/ /g ;
$page =~ s/^.*matching results in database// ;

while ( $page =~ m|<p>\s*<a href="(http://www.newulsterbiography.co.uk/index.php/home/viewPerson/)(\d+)">(.+?)</a>(.*?)</p>|gm ) {
	my $out = {
		id => $2 ,
		url => "$1$2" ,
		name => $3 ,
		desc => $4 ,
		aux => [] ,
		type => 'person'
	} ;
	$out->{name} =~ s/^(.+?), (.+)$/$2 $1/ ;
	$out->{desc} =~ s/^\s+// ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	my $s = encode_json ( $out ) ;
	print OUT $s ;
}

print OUT "\n]}" ;

0 ;
