#!/usr/bin/python
import csv
import json

# https://docs.google.com/spreadsheets/d/1RVVZY00MZNrK2YCTTzVrbTFH2t3RxoAZah128gQR-NM/edit#gid=1200950288

id = 0 ;
j = { 'catalog':92 , 'entries':[] }
with open('FRS.txt') as csvfile:
	reader = csv.reader ( csvfile , delimiter='\t', quotechar='"')
	for row in reader:
		if row[0] == 'First names' :
			continue
		id += 1
		name = row[0] + ' ' + row[1]
		desc = row[2] + ' ' + row[3] + ' ' + row[4] + ' '
		desc += row[8] + ' ' + row[9] + ' ' + row[10] + ' - '
		desc += row[12] + ' ' + row[13] + ' ' + row[14]
		url = ''
		j['entries'].append ( { 'id':id , 'name':name , 'desc':desc , 'url':url , 'aux':[] , 'type':'person' } )

f = open('out.json','w')
f.write ( json.dumps ( j ) )
f.close()
