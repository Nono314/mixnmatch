#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

`wget -O data.csv 'https://doi.crossref.org/funderNames?mode=list'` ;

open OUT, "> out.json" or die $! ;

my $first = 1 ;
print OUT '{"catalog":62,"entries":' ;
print OUT "[\n" ;
open FILE , 'data.csv' ;
while ( <FILE> ) {
	chomp ;
	next unless $_ =~ m|^(http.+?/)(\d+),"(.+)",\s*$| ;
	my $out = {
		'name' => $3 ,
		'id' => $2 ,
		'url' => "$1$2" ,
		'desc' => '' ,
		'aux' => []
	} ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
}
close FILE ;
print OUT "\n]}" ;

0 ;
