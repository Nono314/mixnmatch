#!/usr/bin/php
<?PHP

$page = file_get_contents ( 'http://www.volcano.si.edu/list_volcano_holocene.cfm' ) ;
$page = preg_replace ( '/\s+/' , ' ' , $page ) ;
preg_match_all ( '/<tr>\s*<td><a href="volcano\.cfm\?vn=(\d+)">(.+?)<\/a><\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<\/tr>/' , $page , $m ) ;

$out = array ( 'catalog' => 57 , 'entries' => array() ) ;

foreach ( $m[0] AS $k => $dummy ) {
	$out['entries'][] = array (
		'id' => $m[1][$k] ,
		'name' => $m[2][$k] ,
		'desc' => $m[3][$k] . ' / ' . $m[4][$k] . ' / ' . $m[5][$k] ,
		'url' => 'http://www.volcano.si.edu/volcano.cfm?vn='.$m[1][$k] ,
		'type' => 'location' ,
		'aux' => array()
	) ;
}

$fh = fopen ( "out.json" , 'w' ) ;
fwrite ( $fh , json_encode ( $out ) ) ;
fclose ( $fh ) ;


?>