#!/usr/bin/python
import csv
import json

# external_id,name,description,url,location,coordinates,style,Completion,"other names","Part of"
# print OUT '{"catalog":' . $catalog_id . ',"entries":' ;


j = { 'catalog':81 , 'entries':[] }
with open('data.tab') as csvfile:
	reader = csv.reader ( csvfile , delimiter=',', quotechar='"')
	for row in reader:
		while len(row) < 5:
			row.append('')
		id = row[0].strip()
		name = row[1].strip()
		desc = row[2].strip()
#		if row[3] != '':
#			desc += "; born " + row[3].strip()
#		if row[4] != '':
#			desc += "; died " + row[4].strip()
		n = name.split ( ', ' , 1 )
		if len(n) > 1:
			name = n[1].strip() + ' ' + n[0].strip()
		url = 'http://www.haw.uni-heidelberg.de/akademie/member.en.html?id=' + id
		j['entries'].append ( { 'id':id , 'name':name , 'desc':desc , 'url':url , 'aux':[] , 'type':'person' } )

f = open('out.json','w')
f.write ( json.dumps ( j ) )
f.close()
