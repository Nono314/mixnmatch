#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;

my %ids ;
open OUT, "> out.json" or die $! ;

my $first = 1 ;
my $fin ;
print OUT '{"catalog":15,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 'a' .. 'z' ) {
	my $num = 0 ;
	my $first_page = '' ;
	$fin = 0 ;
	while ( $num < 7500 and not $fin ) {
		my $url = "http://www.biografischportaal.nl/personen?start=$num&beginletter=$letter" ;
		my $page = get $url ;
		if ( $num == 0 ) {
			$first_page = $page ;
		} else {
			last if $first_page eq $page ;
		}
		process_page ( $page ) ;
		$num += 30 ;
#		last ;
	}
#	last ;
}
print OUT "\n]}" ;

0 ;

sub process_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	
	my $out = get_blank() ;
	
	foreach my $r ( @rows ) {

		if ( $r =~ m/class="persoon"/ ) {
			$out = flushPerson($out) ;
			next ;
		}
	
		if ( $r =~ m/class="list_of_persons"/ ) {
			$out = get_blank() ;
			next ;
		}
	
		if ( $r =~ m/class="prev_next_navigation"/ ) {
			$out = flushPerson($out) ;
			next ;
		}
	
		if ( $r =~ m|(http://www.biografischportaal.nl/persoon/)(.+)">(.+)</a>| ) {
			$out->{id} = $2 ;
			$out->{url} = "$1$2" ;
			$out->{name} = $3 ;
			next ;
		}
	
		if ( $r =~ m|<span class="date">(.+)</span>| or $r =~ m|<span class="snippet">(.+)</span>|  ) {
			$out->{desc} .= ' ' . $1 ;
			next ;
		}
		last if $fin ;
	}
	flushPerson ( $out ) ;
}

sub flushPerson {
	my ( $out ) = @_ ;
	my $ret = get_blank() ;
	return $ret unless defined $out->{id} ;
	return $ret if $out->{id} eq '' ;
	
	if ( defined $ids{$out->{id}} ) {
		$fin = 1 ;
		return $ret ;
	}
	$ids{$out->{id}} = 1 ;
	
	$out->{name} =~ s/^([^,]+), ([^,]+)$/$2 $1/ ;
	$out->{desc} =~ s/^\s+// ;
	$out->{desc} =~ s/\s+$// ;
	
	if ( $first ) { $first = 0 ; }
	else { print OUT ",\n" ; }
	print OUT encode_json ( $out ) ;
	return $ret ;
}

sub get_blank {
	return { "aux" => [] , "type" => "person" , "name" => '' , "url" => '' , "desc" => '' } ;
}
