#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;
print OUT '{"catalog":52,"entries":' ;
print OUT "[\n" ;

my %ids ;
my $first = 1 ;
my $out ;
foreach my $letter ( 'a' .. 'z' ) {
	my $page = 1 ;
	while ( 1 ) {
		my $url = "https://www.artsy.net/artists/artists-starting-with-$letter?page=$page" ;
		my $html = get $url ;
		last unless defined $html ;
		my $cnt = parse ( $html ) ;
		last if $cnt == 0 ;
		$page++ ;
	}
}

print OUT "\n]}" ;

0 ;

sub parse {
	my ( $page ) = @_ ;

	my $cnt = 0 ;
	while ( $page =~ m|<a href="/artist/(.+?)" class="highlight-link">(.+?)</a>|g ) {
		my $out = get_blank() ;
		$out->{id} = $1 ;
		$out->{url} = "https://www.artsy.net/artist/$1" ;
		$out->{name} = $2 ;
		
		next if defined $ids{$out->{id}} ;
		$ids{$out->{id}} = 1 ;
		
		if ( $first ) { $first = 0 ; }
		else { print OUT ",\n" ; }
		print OUT encode_json ( $out ) ;
		$cnt++ ;
	}

	return $cnt ;
}

sub get_blank {
	return { desc => '' , aux => [] , type => 'person' } ;
}