#!/usr/bin/php
<?PHP

require_once ( "../../public_html/php/common.php" ) ;

$db = openToolDB ( 'mixnmatch_p' , 'wikidatawiki.labsdb' , 'p50380g50851' ) ;
#$dbwd = openDB ( 'wikidata' , 'wikidata' ) ;
$db->set_charset("utf8") ;

$entries = array() ;
$sql = "SELECT * FROM entry WHERE catalog=16 AND ext_desc=''" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()) $entries[] = $o ;

foreach ( $entries AS $o ) {
//	print $o->ext_name . "\n" ;
	$html = file_get_contents ( $o->ext_url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if ( !preg_match ( '/<h1.*?>(.+?)<\/h1>/' , $html , $m ) ) continue ;
	$desc = array_pop ( $m ) ;
	$desc = str_replace ( '&#8202;' , '' , $desc ) ;
	$desc = preg_replace ( '/<.+?>/' , '' , $desc ) ;
	$desc = preg_replace ( '/'.$o->ext_name.'\s*/' , '' , $desc ) ;
	$desc = preg_replace ( '/[()]/' , '' , $desc ) ;
	$sql = "UPDATE entry SET ext_desc='" . $db->real_escape_string(trim($desc)) . "' WHERE id=" . $o->id ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
}

?>