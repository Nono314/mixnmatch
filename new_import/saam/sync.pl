#!/usr/bin/perl

use strict ;
use warnings ;
use utf8 ;
use LWP::Simple;
use Data::Dumper ;
use JSON ;


open OUT, "> out.json" or die $! ;

my $last_id = '' ;
my $first = 1 ;
print OUT '{"catalog":39,"entries":' ;
print OUT "[\n" ;
foreach my $letter ( 65 .. 90 ) {
	my $url = "http://americanart.si.edu/collections/search/artist/results/?q=$letter&b=0&p=0" ;
	my $page = get $url ;
	parse_page ( $page ) ;
}
print OUT "\n]}" ;

0 ;

sub parse_page {
	my ( $page ) = @_ ;
	my @rows = split "\n" , $page ;
	my $out = get_blank() ;
	foreach my $r ( @rows ) {

		if ( $r =~ m|<a href="(/collections/search/artist/\?id=)(\d+)">| )  {
			$out->{id} = $2 ;
			$out->{url} = "http://americanart.si.edu$1$2" ;
		} elsif ( $r =~ m|<span class="artistName">(.+?)</span>| )  {
			$out->{name} = $1 ;
			$out->{name} =~ s|^(.+), ([^,]+)$|$2 $1| ;
		} elsif ( $r =~ m|<span class="artistDescription">(.+?)</span>| )  {
			$out->{desc} = $1 ;
			$out->{type} = 'person' if $out->{desc} =~ m/(born|died)/ ;
		} elsif ( $r =~ m|</{0,1}li>| )  {
			if ( defined $out->{id} ) {
				if ( $first ) { $first = 0 ; }
				else { print OUT ",\n" ; }
				print OUT encode_json ( $out ) ;
			}
			$out = get_blank() ;
		}
	}
}

sub get_blank {
	return { desc => '' , aux => [] , type => '' } ;
}